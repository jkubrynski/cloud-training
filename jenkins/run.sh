#!/bin/bash
docker run -p 8080:8080 -v jenkins_home:/var/jenkins_home --env JAVA_OPTS=-Djenkins.install.runSetupWizard=false -e AZURE_CLIENT_ID=$ARM_CLIENT_ID -e AZURE_SUBSCRIPTION_ID=$ARM_SUBSCRIPTION_ID -e AZURE_TENANT=$ARM_TENANT_ID -e AZURE_SECRET=$ARM_CLIENT_SECRET cloud_jenkins
