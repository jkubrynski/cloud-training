import hudson.security.AuthorizationStrategy
import jenkins.model.Jenkins
import hudson.model.FreeStyleProject
import javaposse.jobdsl.dsl.DslScriptLoader
import javaposse.jobdsl.plugin.JenkinsJobManagement
import hudson.tasks.Maven
import hudson.tools.InstallSourceProperty

def instance = Jenkins.instance

instance.setAuthorizationStrategy(AuthorizationStrategy.UNSECURED)

instance.save()

def jobManagement = new JenkinsJobManagement(System.out, [:], new File('.'))

def seedJobScript = new File('/usr/share/jenkins/seed.groovy')

new DslScriptLoader(jobManagement).with {
	runScript(seedJobScript.text)
}

def maven = instance.getDescriptorByType(Maven.DescriptorImpl)
def installer = new Maven.MavenInstaller('3.5.0')
def installerProps = new InstallSourceProperty([installer])
instalations=(maven.installations as List)
instalations.add(new Maven.MavenInstallation("maven", "/var/jenkins_home/tools/maven", [installerProps]))
maven.installations = instalations
maven.save()
