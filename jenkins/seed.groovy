job('seed-job') {
    scm {
        git {
            remote {
                url('https://bitbucket.org/jkubrynski/cloud-training.git')
            }
            branch('master')
        }
    }
    steps {
        dsl {
            external('jobs/*.groovy')
            removeAction('DISABLE')
            removeViewAction('DELETE')
            ignoreExisting(false)
        }
    }
}