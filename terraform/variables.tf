variable "location" {
  description = "Specifies the location"
  default     = "westeurope"
}

variable "admin_username" {
  description = "Specifies the admin username"
  default     = "infra"
}

variable "admin_password" {
  description = "Specifies the admin password"
  default     = "TestInfra123"
}

variable "vm_size" {
  description = "Specifies the size of the virtual machine"
  default     = "Standard_A1"
}

variable "image_publisher" {
  description = "name of the publisher of the image (az vm image list)"
  default     = "OpenLogic"
}

variable "image_offer" {
  description = "the name of the offer (az vm image list)"
  default     = "CentOS"
}

variable "image_sku" {
  description = "image sku to apply (az vm image list)"
  default     = "7.3"
}

variable "image_version" {
  description = "version of the image to apply (az vm image list)"
  default     = "latest"
}
