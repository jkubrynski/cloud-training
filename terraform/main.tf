provider "azurerm" {
}

resource "azurerm_resource_group" "test" {
  name     = "kub-weu-test-rg"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "vnet" {
  name                = "kub-weu-test-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.test.name}"

}
resource "azurerm_subnet" "app" {
  name                 = "kub-weu-test-vnet-app"
  resource_group_name  = "${azurerm_resource_group.test.name}"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  address_prefix       = "10.0.1.0/24"
}

resource "azurerm_availability_set" "sd" {
  name                = "kub-weu-test-sd-as"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.test.name}"
  managed             = true
}

resource "azurerm_public_ip" "sd-1" {
  name                         = "kub-weu-test-sd-ip-1"
  location                     = "${var.location}"
  resource_group_name          = "${azurerm_resource_group.test.name}"
  public_ip_address_allocation = "dynamic"
}

resource "azurerm_network_interface" "sd-1" {
  name                      = "kub-weu-test-sd-ni-1"
  location                  = "${var.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"

  ip_configuration {
    name                          = "kub-weu-test-sd-ni-cfg-1"
    subnet_id                     = "${azurerm_subnet.app.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.sd-1.id}"
  }
}

resource "azurerm_virtual_machine" "sd-1" {
  name                  = "kub-weu-test-sd-vm-1"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.test.name}"
  network_interface_ids = ["${azurerm_network_interface.sd-1.id}"]
  vm_size               = "${var.vm_size}"
  availability_set_id   = "${azurerm_availability_set.sd.id}"

  storage_image_reference {
    publisher = "${var.image_publisher}"
    offer     = "${var.image_offer}"
    sku       = "${var.image_sku}"
    version   = "${var.image_version}"
  }

 storage_os_disk {
    name              = "kub-weu-test-sd-vm-1-disk-os"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "k-weu-sd1"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags {
    component = "sd"
  }
}

resource "azurerm_public_ip" "sd-2" {
  name                         = "kub-weu-test-sd-ip-2"
  location                     = "${var.location}"
  resource_group_name          = "${azurerm_resource_group.test.name}"
  public_ip_address_allocation = "dynamic"
}

resource "azurerm_network_interface" "sd-2" {
  name                      = "kub-weu-test-sd-ni-2"
  location                  = "${var.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"

  ip_configuration {
    name                          = "kub-weu-test-sd-ni-cfg-2"
    subnet_id                     = "${azurerm_subnet.app.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.sd-2.id}"
  }
}

resource "azurerm_virtual_machine" "sd-2" {
  name                  = "kub-weu-test-sd-vm-2"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.test.name}"
  network_interface_ids = ["${azurerm_network_interface.sd-2.id}"]
  vm_size               = "${var.vm_size}"
  availability_set_id   = "${azurerm_availability_set.sd.id}"

  storage_image_reference {
    publisher = "${var.image_publisher}"
    offer     = "${var.image_offer}"
    sku       = "${var.image_sku}"
    version   = "${var.image_version}"
  }

 storage_os_disk {
    name              = "kub-weu-test-sd-vm-2-disk-os"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "k-weu-sd2"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags {
    component = "sd"
  }
}


resource "azurerm_availability_set" "tower" {
  name                = "kub-weu-test-tower-as"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.test.name}"
  managed             = true
}

resource "azurerm_public_ip" "tower-1" {
  name                         = "kub-weu-test-tower-ip-1"
  location                     = "${var.location}"
  resource_group_name          = "${azurerm_resource_group.test.name}"
  public_ip_address_allocation = "dynamic"
}

resource "azurerm_network_interface" "tower-1" {
  name                      = "kub-weu-test-tower-ni-1"
  location                  = "${var.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"

  ip_configuration {
    name                          = "kub-weu-test-tower-ni-cfg-1"
    subnet_id                     = "${azurerm_subnet.app.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.tower-1.id}"
  }
}

resource "azurerm_virtual_machine" "tower-1" {
  name                  = "kub-weu-test-tower-vm-1"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.test.name}"
  network_interface_ids = ["${azurerm_network_interface.tower-1.id}"]
  vm_size               = "${var.vm_size}"
  availability_set_id   = "${azurerm_availability_set.tower.id}"

  storage_image_reference {
    publisher = "${var.image_publisher}"
    offer     = "${var.image_offer}"
    sku       = "${var.image_sku}"
    version   = "${var.image_version}"
  }

 storage_os_disk {
    name              = "kub-weu-test-tower-vm-1-disk-os"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "k-weu-tower1"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags {
    component = "tower"
  }
}




resource "azurerm_availability_set" "station" {
  name                = "kub-weu-test-station-as"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.test.name}"
  managed             = true
}

resource "azurerm_public_ip" "station-1" {
  name                         = "kub-weu-test-station-ip-1"
  location                     = "${var.location}"
  resource_group_name          = "${azurerm_resource_group.test.name}"
  public_ip_address_allocation = "dynamic"
}

resource "azurerm_network_interface" "station-1" {
  name                      = "kub-weu-test-station-ni-1"
  location                  = "${var.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"

  ip_configuration {
    name                          = "kub-weu-test-station-ni-cfg-1"
    subnet_id                     = "${azurerm_subnet.app.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.station-1.id}"
  }
}

resource "azurerm_virtual_machine" "station-1" {
  name                  = "kub-weu-test-station-vm-1"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.test.name}"
  network_interface_ids = ["${azurerm_network_interface.station-1.id}"]
  vm_size               = "${var.vm_size}"
  availability_set_id   = "${azurerm_availability_set.station.id}"

  storage_image_reference {
    publisher = "${var.image_publisher}"
    offer     = "${var.image_offer}"
    sku       = "${var.image_sku}"
    version   = "${var.image_version}"
  }

 storage_os_disk {
    name              = "kub-weu-test-station-vm-1-disk-os"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "k-weu-station1"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags {
    component = "station"
  }
}