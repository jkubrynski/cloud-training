package com.kubrynski.tower.service;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Jakub Kubrynski
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(ids="com.kubrynski.micro:station:+:stubs", workOffline = true)
public class WeatherClientTest {

	@Autowired
	StationClient stationClient;

	@Test
	public void shouldRetrieveWeather() throws Exception {
		//when
		Forecast forecast = stationClient.getForecast();

		//then
		assertThat(forecast.getCondition()).isEqualTo(Condition.GOOD);
	}

}