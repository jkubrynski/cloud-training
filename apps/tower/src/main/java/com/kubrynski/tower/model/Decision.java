package com.kubrynski.tower.model;

/**
 * @author Jakub Kubrynski
 */
public class Decision {

	private final String state;

	private Decision(final String state) {
		this.state = state;
	}

	public String getState() {
		return state;
	}

	public static Decision unknown() {
		return new Decision("unknown");
	}

	public static Decision land() {
		return new Decision("land");
	}

	public static Decision hold() {
		return new Decision("hold");
	}
}
