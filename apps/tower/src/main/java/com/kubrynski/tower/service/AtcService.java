package com.kubrynski.tower.service;

import java.lang.invoke.MethodHandles;

import com.kubrynski.tower.model.Decision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

/**
 * @author Jakub Kubrynski
 */
@Service
public class AtcService {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final StationClient stationClient;

	public AtcService(final StationClient stationClient) {
		this.stationClient = stationClient;
	}

	public Decision decide() {
		LOG.info("Invoking service");
		Forecast forecastResponse = stationClient.getForecast();
		Condition condition = forecastResponse.getCondition();
		if (condition == Condition.GOOD) {
			return Decision.land();
		} else if (condition == Condition.UNKNOWN){
			return Decision.unknown();
		} else {
			return Decision.hold();
		}
	}
}
