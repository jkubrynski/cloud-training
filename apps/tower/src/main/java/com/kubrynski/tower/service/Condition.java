package com.kubrynski.tower.service;

/**
 * @author Jakub Kubrynski
 */
enum Condition {
	GOOD, BAD, UNKNOWN
}
