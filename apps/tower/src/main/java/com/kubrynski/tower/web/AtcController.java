package com.kubrynski.tower.web;

import java.lang.invoke.MethodHandles;

import com.kubrynski.tower.model.Decision;
import com.kubrynski.tower.service.AtcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jakub Kubrynski
 */
@RestController
@RequestMapping("/atc")
class AtcController {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final AtcService atcService;

	AtcController(final AtcService atcService) {
		this.atcService = atcService;
	}

	@GetMapping
	Decision checkDecision() {
		LOG.info("Checking decision");
		return atcService.decide();
	}
}
