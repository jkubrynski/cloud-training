package com.kubrynski.tower.service;

import org.springframework.stereotype.Component;

/**
 * @author Jakub Kubrynski
 */
@Component
public class StationClientFallback implements StationClient {

	@Override
	public Forecast getForecast() {
		Forecast forecast = new Forecast();
		forecast.setCondition(Condition.UNKNOWN);
		return forecast;
	}
}
