package com.kubrynski.tower.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Jakub Kubrynski
 */
@FeignClient(value = "station", fallback = StationClientFallback.class)
public interface StationClient {

	@RequestMapping(method = RequestMethod.GET, value = "/forecast", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Forecast getForecast();
}
