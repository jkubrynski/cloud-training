package com.kubrynski.tower.web;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.kubrynski.tower.model.Condition;
import com.kubrynski.tower.model.Forecast;
import com.kubrynski.tower.service.ForecastService;
import org.junit.Before;
import org.mockito.Mockito;

/**
 * @author Jakub Kubrynski
 */
public class AbstractContractTest {

	@Before
	public void setUp() throws Exception {
		ForecastService forecastService = Mockito.mock(ForecastService.class);
		Mockito.when(forecastService.currentForecast()).thenReturn(new Forecast(Condition.GOOD));
		RestAssuredMockMvc.standaloneSetup(new ForecastController(forecastService));
	}
}
