package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	request {
		url '/forecast'
		method 'GET'
		headers {
			header 'Accept' : 'application/json;charset=UTF-8'
		}
	}

	response {
		status 200
		body(
				"condition" : "GOOD"
		)
		headers {
			header 'Content-Type' : 'application/json;charset=UTF-8'
		}
	}
}