package com.kubrynski.tower.service;


import java.lang.invoke.MethodHandles;

import com.kubrynski.tower.model.Condition;
import com.kubrynski.tower.model.Forecast;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

/**
 * @author Jakub Kubrynski
 */
@Service
public class ForecastService {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public Forecast currentForecast() {
		LOG.info("Producing forecast");
		return new Forecast(Condition.GOOD);
	}
}
