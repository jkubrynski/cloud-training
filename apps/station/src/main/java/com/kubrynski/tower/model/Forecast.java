package com.kubrynski.tower.model;

/**
 * @author Jakub Kubrynski
 */
public class Forecast {

	private final Condition condition;

	public Forecast(final Condition condition) {
		this.condition = condition;
	}

	public Condition getCondition() {
		return condition;
	}
}
