package com.kubrynski.tower.model;

/**
 * @author Jakub Kubrynski
 */
public enum Condition {
	GOOD, BAD
}
