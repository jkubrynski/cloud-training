package com.kubrynski.tower.web;

import java.lang.invoke.MethodHandles;

import com.kubrynski.tower.model.Forecast;
import com.kubrynski.tower.service.ForecastService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jakub Kubrynski
 */
@RestController
@RequestMapping("/forecast")
class ForecastController {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final ForecastService forecastService;

	ForecastController(final ForecastService forecastService) {
		this.forecastService = forecastService;
	}

	@GetMapping
	Forecast checkDecision() {
		LOG.info("Checking forecast");
		return forecastService.currentForecast();
	}
}
