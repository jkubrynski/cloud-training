# Before you start

## Environment setup

### Software installation

Please make sure that you have following packages installed:

 * ansible >2.2.1
 * python-azure 2.0.0.rc5

You can use pip to install them globally in your system:

```
$ sudo pip install -r requirements.txt
```