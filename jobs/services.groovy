import javaposse.jobdsl.dsl.DslFactory

def dslFactory = this as DslFactory

def services = ['sd','station','tower']
services.each { item ->
    GString buildJobName = "${item}-build"
    GString deployJobName = "${item}-deploy"
    freeStyleJob(deployJobName) {
        quietPeriod(0)
        scm {
            git {
                remote {
                    url('https://bitbucket.org/jkubrynski/cloud-training.git')
                }
            }
        }

        steps {
            copyArtifacts(buildJobName) {
                includePatterns("apps/${item}/target/${item}.jar")
                targetDirectory("ansible/deploys/")
                flatten()
                buildSelector {
                    upstreamBuild()
                }
            }
            shell("cd ansible\nansible-playbook deploys/${item}.yml")
        }
        deliveryPipelineConfiguration("Deploy", "Deploy")
    }

    freeStyleJob(buildJobName) {
        quietPeriod(0)
        scm {
            git {
                remote {
                    url('https://bitbucket.org/jkubrynski/cloud-training.git')
                }
                extensions {
                    cleanAfterCheckout()
                }
            }
        }
        steps {
            maven {
                goals('clean install')
                mavenInstallation('maven')
                rootPOM("apps/${item}/pom.xml")
            }
            downstreamParameterized {
                trigger(deployJobName)
            }
        }
        publishers {
            archiveArtifacts {
                pattern("apps/${item}/target/${item}.jar")
                onlyIfSuccessful()
            }
        }
        deliveryPipelineConfiguration("Build", "Build")
    }

    dslFactory.deliveryPipelineView("${item}") {
        allowPipelineStart()
        allowRebuild()
        showAggregatedPipeline(false)
        updateInterval(5)
        enableManualTriggers()
        showAvatars()
        showChangeLog()
        showDescription()
        showPromotions()
        showTotalBuildTime()
        enablePaging()
        pipelines {
            component("Deployment", buildJobName)
        }
    }
}
